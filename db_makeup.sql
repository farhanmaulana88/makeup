-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2019 at 06:49 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_makeup`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_barang`
--

CREATE TABLE `tb_barang` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_barang`
--

INSERT INTO `tb_barang` (`id`, `nama`, `deskripsi`, `harga`, `quantity`, `gambar`) VALUES
(2, 'Body Mist', 'Dengan wewangian khas dari bunga Jasmine yang mampu memberikan kesan elegan dan berkelas. Memiliki kualitas wangi yang tahan lama, sehingga mampu menemani keseharianmu sepanjang hari.\r\n\r\n', 95000, 29, 'body_mist1.jpg'),
(3, 'Two Way Cake', 'Two way Cake ini cocok untuk jenis muka dry dan oily karena diperkaya dengan minyak zaitun,menghasilkan kelembapan dan membuat kulit terasa lebih moist dan muka lebih jadi mulus dan bebas kilap.', 149000, 48, 'bedak.jpg'),
(4, 'Lip Cream', 'lip cream merupakan produk pertama dari rangkaian produk kecantikan terbaru dari Rossa. Menampilkan pewarna bibir dengan matte finish, mengandung moisturizer serta bertekstur ringan sehingga terasa nyaman saat dipakai seharian dan memberikan natural look pada bibir anda. Rossa Lasts matte lip cream ini longlasting dan transfer proof menjadikan warna bibir anda akan terus bertahan sampai akhirnya anda memutuskan untuk menghapusnya', 120000, 30, 'lip_cream1.jpg'),
(5, 'Lip Tease', 'Lip Tease adalah pewarna bibir berbentuk stick dengan hasil akhir berbentuk creamy matte. Mengandung vitamin E dan vitamin C yang mempercantik serta melindungi bibir agar tidak kering.', 68000, 48, 'lip_tease1.jpg'),
(6, 'Parfume My Love', 'Lembut dan Romantis, itulah kesan yang akan Anda dapatkan dalam wewangian My Love. Perpaduan essential oil musk, sandalwood, strawberry, lily dan caramel yang manis menghasilkan wangi khas dan memperkuat sisi feminin anda. Balut tubuhmu dengan keharuman My Love dan biarkan mereka jatuh cinta.', 348000, 48, 'parfume1.jpg'),
(7, 'Sorbet', 'Sorbet adalah pewarna bibir berbentuk krim dengan tekstur yang creamy dengan kandungan Vitamin C & Vitamin E yang berfungsi sebagai anti-oksidan dan juga dapat membuat bibir terlihat sehat. Selain itu, dengan sensasi dingin pada lip cream ini tentu akan membuat bibir Anda terasa lebih segar sepanjang hari. Lip Cream ini memiliki 3 fungsi yaitu Lip Color, Blush On dan Eye Shadow. Satu produk yang bisa memaksimalkan kecantikan kamu.', 68000, 42, 'sorbet1.jpg'),
(8, 'lip mate', 'lip mate adalah produk rossa', 95000, 10, 'lip_cream2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_chat`
--

CREATE TABLE `tb_chat` (
  `id` int(11) NOT NULL,
  `id_pengirim` int(11) NOT NULL,
  `id_penerima` int(11) NOT NULL,
  `id_conversation` int(11) NOT NULL,
  `pesan` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_chat`
--

INSERT INTO `tb_chat` (`id`, `id_pengirim`, `id_penerima`, `id_conversation`, `pesan`, `tanggal`) VALUES
(1, 1, 2, 1, 'test', '2019-10-13 16:35:53'),
(3, 2, 1, 1, 'yaww', '2019-10-13 16:42:17'),
(4, 3, 2, 3, 'min', '2019-10-13 16:42:54'),
(5, 2, 3, 3, 'uyy', '2019-10-13 16:43:06');

-- --------------------------------------------------------

--
-- Table structure for table `tb_conversation`
--

CREATE TABLE `tb_conversation` (
  `id` int(11) NOT NULL,
  `id_user2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_conversation`
--

INSERT INTO `tb_conversation` (`id`, `id_user2`) VALUES
(1, 2),
(2, 2),
(3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_komen`
--

CREATE TABLE `tb_komen` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `komen` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_komen`
--

INSERT INTO `tb_komen` (`id`, `id_user`, `id_barang`, `komen`, `tanggal`) VALUES
(1, 1, 3, 'qwe', '2019-10-13 10:38:14'),
(2, 1, 3, 'barangnya bagus sekali gan', '2019-10-13 10:38:40');

-- --------------------------------------------------------

--
-- Table structure for table `tb_nama`
--

CREATE TABLE `tb_nama` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_nama`
--

INSERT INTO `tb_nama` (`id`, `nama_lengkap`) VALUES
(1, 'Farhan Maulana'),
(2, 'Admin'),
(3, 'Wulan Dwi Hartati');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembayaran`
--

CREATE TABLE `tb_pembayaran` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `bukti` text NOT NULL,
  `total` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pembayaran`
--

INSERT INTO `tb_pembayaran` (`id`, `id_user`, `bukti`, `total`, `status`) VALUES
(1, 1, 'DVhS6n8VAAERv8X.jpg', 602003, 'Lunas'),
(2, 1, 'Struk-ATM-Parkir-1024x7151.jpg', 204001, 'Lunas'),
(19, 1, '', 393000, 'Lunas');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembelian`
--

CREATE TABLE `tb_pembelian` (
  `id` int(11) NOT NULL,
  `id_pembayaran` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `status` enum('Diproses','Dikirim','Diterima') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pembelian`
--

INSERT INTO `tb_pembelian` (`id`, `id_pembayaran`, `id_user`, `id_barang`, `jumlah`, `status`) VALUES
(1, 1, 1, 2, 3, 'Dikirim'),
(2, 1, 1, 3, 2, 'Diterima'),
(3, 2, 1, 2, 1, 'Diproses'),
(4, 2, 1, 3, 4, 'Diproses'),
(37, 19, 1, 2, 1, 'Diproses'),
(38, 19, 1, 3, 2, 'Diproses');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_lengkap` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `level` enum('admin','pembeli','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `nama_lengkap`, `alamat`, `level`) VALUES
(1, 'farhan', 'farhan', 'Farhan Maulana', 'Bandung', 'pembeli'),
(2, 'admin', 'admin', 'Admin', 'Bandung', 'admin'),
(3, 'wulan', 'wulan', 'Wulan Dwi Hartati', 'Sumedang', 'pembeli');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_chat`
--
ALTER TABLE `tb_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_conversation`
--
ALTER TABLE `tb_conversation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_komen`
--
ALTER TABLE `tb_komen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_nama`
--
ALTER TABLE `tb_nama`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pembelian`
--
ALTER TABLE `tb_pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_barang`
--
ALTER TABLE `tb_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_chat`
--
ALTER TABLE `tb_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_conversation`
--
ALTER TABLE `tb_conversation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_komen`
--
ALTER TABLE `tb_komen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_nama`
--
ALTER TABLE `tb_nama`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tb_pembelian`
--
ALTER TABLE `tb_pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
