<?php
class ModelAdmin extends ci_model{
	
	public function __construct(){
		$this->load->database();
    	$this->load->helper('url');
	}


    public function getBarang()
    {
        return $this->db->get('tb_barang')->result();
    }

    public function getTotal()
    {
        $status = 'Lunas';
        $this->db->select_sum('total');
        $this->db->from('tb_pembayaran');
        $this->db->where('status', $status);
        return $this->db->get()->result();
    }

    public function getUser()
    {
        $data = $this->db->query("SELECT * FROM tb_user WHERE level != 'admin'");
        return $data->result();
        // return $this->db->get('tb_user')->result();
    }

    public function getChat($id)
    {
        $this->db->from('orders');
        $data = $this->db->query("SELECT tb_user.id, tb_nama.nama_lengkap as nama, tb_user.nama_lengkap, tb_chat.pesan, tb_chat.tanggal FROM tb_chat JOIN tb_user ON tb_chat.id_pengirim = tb_user.id JOIN tb_nama ON tb_chat.id_penerima = tb_nama.id JOIN tb_conversation ON tb_chat.id_conversation = tb_conversation.id WHERE tb_conversation.id = $id OR id_conversation = '2' ORDER BY tanggal ASC");
        return $data->result();
        // print_r($id); die;
        //         $this->db->select('tb_user.id, tb_nama.nama_lengkap as nama, tb_user.nama_lengkap, tb_chat.pesan, tb_chat.tanggal');
        //     $this->db->join('tb_user','tb_chat.id_pengirim = tb_user.id');
        //     $this->db->join('tb_nama','tb_chat.id_penerima = tb_nama.id');
        //     $this->db->join('tb_conversation','tb_chat.id_conversation = tb_conversation.id');
        //     $this->db->order_by('tanggal','DESC');
        // $asd = $this->db->get_where('tb_chat', array('tb_conversation.id' => $id))->result();
        // print_r($asd); die;
    }

    public function chatAdmin($data)
    {
    return $this->db->insert('tb_chat',$data);
    }

    public function insertBarang($img_name)
    {
    $data = array(
      'gambar' => $img_name,
      'nama' => $this->input->post('nama'),
      'quantity' => $this->input->post('quantity'),
      'harga' => $this->input->post('harga'),
      'deskripsi' => $this->input->post('deskripsi')
    );
    return $this->db->insert('tb_barang',$data);
    }

    function updateBarang($id,$data){
    // print_r($u); die;
    return  $this->db->update('tb_barang',$data,array('id'=>$id));  
  	} 

    public function deleteBarang($where)
    {
    $this->db->where($where);
    $this->db->delete('tb_barang');
  	}

     function getPesanan()
    {
        // $status = 'Already Received';
        $this->db->select('tb_pembelian.id, tb_pembayaran.status,tb_barang.nama,tb_pembelian.jumlah,tb_user.nama_lengkap,tb_user.alamat, tb_pembelian.status as pengiriman');
        // $this->db->from('orders');
            $this->db->join('tb_barang','tb_pembelian.id_barang = tb_barang.id');
            $this->db->join('tb_user','tb_pembelian.id_user = tb_user.id');
            $this->db->join('tb_pembayaran','tb_pembelian.id_pembayaran = tb_pembayaran.id');
        $data = $this->db->get('tb_pembelian'); // menampilkan data orderan yang id user nya sama dengan session toko id
        return $data->result();
    }

    function getPembayaran()
    {
        // $status = 'Already Received';
        $this->db->select('tb_pembayaran.id,tb_pembayaran.bukti,tb_user.nama_lengkap,tb_pembayaran.total,tb_pembayaran.status');
        // $this->db->from('orders');
            $this->db->join('tb_user','tb_pembayaran.id_user = tb_user.id');
        $data = $this->db->get('tb_pembayaran'); // menampilkan data orderan yang id user nya sama dengan session toko id
        return $data->result();
    }

    public function updateStatus($id)
    {
        $a = 'Lunas';
        $data = $this->db->query("UPDATE tb_pembayaran set status = '$a' Where id = $id");
        return $data;
    }

    public function updatePengiriman($id)
    {
        $a = 'Dikirim';
        $data = $this->db->query("UPDATE tb_pembelian set status = '$a' Where id = $id");
        return $data;
    }

}