<?php
class ModelLogin extends ci_model{
	
	public function __construct(){
		$this->load->database();
    	$this->load->helper('url');
	}

    function cekLogin($data){      
        return $this->db->get_where('tb_user',$data);
    }

    public function cekPendaftaran($data)
    {
    return $this->db->insert('tb_user',$data);
    }

    public function cekPendaftaran2()
    {
        $data = array(
            'nama_lengkap' => $this->input->post('nama_lengkap') 
        );
    return $this->db->insert('tb_nama',$data);
    }

    public function cekPendaftaran3()
    {
        $data = array(
            'id_user2' => '2',
        );
    return $this->db->insert('tb_conversation',$data);
    }

    function updateBarang($id,$data){
    // print_r($u); die;
    return  $this->db->update('tb_barang',$data,array('id'=>$id));  
  	} 

    public function deleteBarang($where)
    {
    $this->db->where($where);
    $this->db->delete('tb_barang');
  	}

}