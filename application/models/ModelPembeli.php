<?php
class ModelPembeli extends ci_model{
	
	public function __construct(){
		$this->load->database();
    	$this->load->helper('url');
	}


    public function getBarang()
    {
        return $this->db->get('tb_barang')->result();
    }

    public function getKomen($id_barang)
    {
        // $this->db->from('orders');
                $this->db->select('tb_user.nama_lengkap, tb_komen.komen, tb_komen.tanggal');
            $this->db->join('tb_user','tb_komen.id_user = tb_user.id');
            $this->db->order_by('tanggal','DESC');
        return $this->db->get_where('tb_komen', array('id_barang' => $id_barang))->result();
    }

    public function getChat($id)
    {
        $data = $this->db->query("SELECT tb_user.id, tb_nama.nama_lengkap as nama, tb_user.nama_lengkap, tb_chat.pesan, tb_chat.tanggal FROM tb_chat JOIN tb_user ON tb_chat.id_pengirim = tb_user.id JOIN tb_nama ON tb_chat.id_penerima = tb_nama.id JOIN tb_conversation ON tb_chat.id_conversation = tb_conversation.id WHERE tb_conversation.id = $id or id_conversation = '2' ORDER BY tanggal ASC");
        return $data->result();
        // $this->db->from('orders');
        //         $this->db->select('tb_nama.nama_lengkap as nama, tb_user.nama_lengkap, tb_chat.pesan, tb_chat.tanggal');
        //     $this->db->join('tb_user','tb_chat.id_pengirim = tb_user.id');
        //     $this->db->join('tb_nama','tb_chat.id_penerima = tb_nama.id');
        //     $this->db->order_by('tanggal','DESC');
        // return $this->db->get_where('tb_chat', array('id_pengirim' => $id))->result();
    }

     public function chatPembeli($data)
    {
    return $this->db->insert('tb_chat',$data);
    }


    public function insertBarang($img_name)
    {
    $data = array(
      'gambar' => $img_name,
      'nama' => $this->input->post('nama'),
      'quantity' => $this->input->post('quantity'),
      'harga' => $this->input->post('harga')
    );
    return $this->db->insert('tb_barang',$data);
    }

    function komen(){
    $data = array(
        'id_user' => $this->session->userdata('id'),
        'id_barang' => $this->input->post('id_barang'),
        'komen' => $this->input->post('komen')
    );
    $a = $this->db->insert('tb_komen', $data);
    return $a;
  }

    function updateBarang($id,$data){
    // print_r($u); die;
    return  $this->db->update('tb_barang',$data,array('id'=>$id));  
    } 

    public function deleteBarang($where)
    {
    $this->db->where($where);
    $this->db->delete('tb_barang');
    }

    public function detailProduk($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('tb_barang')->result();
    }

    function order(){
            $pembayaran = array(
            'id_user' => $this->session->userdata('id'),    
            'bukti' => '', 
            'total' => $this->cart->total(),  
            'status' => 'Belum Lunas'
        );
        $this->db->insert('tb_pembayaran',$pembayaran);
        $invoice_id = $this->db->insert_id();
        
        foreach($this->cart->contents() as $item){
            $data = array(
                'id_pembayaran' => $invoice_id,
                'id_user' => $this->session->userdata('id'), 
                'id_barang' => $item['id'],
                'jumlah' => $item['qty'],
                'status' => 'Diproses'
            );
            $this->db->insert('tb_pembelian',$data);    
        }
        
        return TRUE;
    }

    public function stock($id_barang,$qtyAkhir)
    {
        $this->db->where('id',$id_barang);
        $data = array(
            'quantity' => $qtyAkhir
        );
       return $this->db->update('tb_barang',$data);
    }

     function riwayatBelanja($id)
    {
        // $status = 'Already Received';
        $this->db->select('tb_pembelian.id, tb_pembelian.id_pembayaran, tb_pembelian.id_user, tb_pembelian.id_barang, tb_pembelian.jumlah, tb_pembelian.status, tb_barang.nama, tb_barang.deskripsi, tb_barang.harga, tb_barang.gambar');
        // $this->db->from('orders');
            $this->db->join('tb_barang','tb_pembelian.id_barang = tb_barang.id');
        $data = $this->db->get_where('tb_pembelian',array('tb_pembelian.id_user' => $id)); // menampilkan data orderan yang id user nya sama dengan session toko id
        return $data->result();
    }

    function riwayatStruk($id)
    {
        // $status = 'Paid';
        $this->db->select('tb_pembayaran.id, tb_user.nama_lengkap, tb_user.alamat, tb_pembayaran.bukti, tb_pembayaran.total, tb_pembayaran.status');
            $this->db->join('tb_user','tb_pembayaran.id_user = tb_user.id');
            // $this->db->order_by("date", "desc");
        $data = $this->db->get_where('tb_pembayaran',array('tb_pembayaran.id_user' => $id)); // menampilkan data orderan yang id user nya sama dengan session toko id
        return $data->result();
    }

    function updateBukti($id,$data){
    // print_r($u); die;
    return  $this->db->update('tb_pembayaran',$data ,array('id'=>$id));  
    } 

    public function updatePengiriman($id)
    {
        // print_r($id);die;
        $a = 'Diterima';
        $data = $this->db->query("UPDATE tb_pembelian set status = '$a' Where id = $id");
        return $data;
    }
}