<?php
Class Index extends CI_Controller{
    
    // var $API ="";
    
    function __construct() {
        parent::__construct();
        // $this->API="http://localhost/uas_bank_server/index.php";
        $this->load->library('session');
        // $this->load->library('curl');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('ModelAdmin');
        $this->load->model('ModelLogin');
    }

    function index(){

        $data['barang'] = $this->ModelAdmin->getBarang();
        // $data['sektor'] = json_decode($this->curl->simple_get($this->API.'/sektor'));


        // $this->load->view('komponen_pembeli/header2');
        $this->load->view('index',$data);
        // $this->load->view('komponen_pembeli/footer2');
        
    }

        function cekLogin(){
        $data = array('username' => $this->input->post('username'),
                        'password' => $this->input->post('password')
            );
        $hasil = $this->ModelLogin->cekLogin($data);
        if ($hasil->num_rows() == 1) {
            foreach ($hasil->result() as $sess) {
                $sess_data['id'] = $sess->id;
                $sess_data['level'] = $sess->level;
                $sess_data['username'] = $sess->username;
                $sess_data['status'] = 'login';
                $this->session->set_userdata($sess_data);
            }
            if ($this->session->userdata('level')=='admin') {
                redirect('admin/getBarang');
            }
            elseif ($this->session->userdata('level')=='pembeli') {
                redirect('pembeli/getBarang');
            }   
        }else{
            echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
        }
        
    }

    function cekPendaftaran(){
          $data = array (
                        'nama_lengkap' => $this->input->post('nama_lengkap'),
                        'username' => $this->input->post('username'),
                        'password' => $this->input->post('password'),
                        'alamat' => $this->input->post('alamat'),
                        'level' => 'pembeli'
                    );
                    $hasil = $this->ModelLogin->cekPendaftaran($data);
                    $hasil2 = $this->ModelLogin->cekPendaftaran2();
                    $hasil3 = $this->ModelLogin->cekPendaftaran3();
                    if (!$hasil) {
                        echo "<script>alert('Gagal Mendaftar! Silahkan Coba Lagi.');</script>";
                        $this->load->view('pendaftaran');
                        // redirect('index/formPendaftaran');
                    }else{
                        echo "<script>alert('Anda Berhasil Mendaftar. Silahkan Lakukan Login.');</script>";
                        $this->load->view('login');
                    }
                    // direct('index/formlogin');
                }

    function logout(){
        // $this->cart->destroy();
        $this->session->sess_destroy();
        redirect('index/formLogin');
    }


    function formLogin(){
        $this->load->view('login');
    }

    function formPendaftaran(){
        $this->load->view('pendaftaran');
    }



}