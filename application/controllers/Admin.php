<?php
Class Admin extends CI_Controller{
    
    var $API ="";
    
    function __construct() {
        parent::__construct();
        $this->API="http://localhost/makeupServer/index.php";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('ModelAdmin');

        if($this->session->userdata('level') != "admin"){
            redirect(base_url("index.php/index/formLogin"));
            }
    }

    public function getBarang()
    {
        // $id_user = $this->session->userdata('id_user');
        // $data['barang'] = $this->ModelAdmin->getBarang();
        $data['barang'] = json_decode($this->curl->simple_get($this->API.'/barang'));

        $this->load->view('komponen_admin/header');
        $this->load->view('komponen_admin/sidebar');
        $this->load->view('admin/barang',$data);
        $this->load->view('komponen_admin/footer');
    }

    public function getPesanan()
    {
        // $id_user = $this->session->userdata('id_user');
        $data['pesanan'] = $this->ModelAdmin->getPesanan();
        // $data['pesanan'] = json_decode($this->curl->simple_get($this->API.'/pesanan'));

        $this->load->view('komponen_admin/header');
        $this->load->view('komponen_admin/sidebar');
        $this->load->view('admin/pesanan',$data);
        $this->load->view('komponen_admin/footer');
    }

    public function getUser()
    {
        // $id_user = $this->session->userdata('id_user');
        $data['user'] = $this->ModelAdmin->getUser();
        // $data['chat'] = $this->ModelAdmin->getChat();
        // $data['pesanan'] = json_decode($this->curl->simple_get($this->API.'/pesanan'));

        $this->load->view('komponen_admin/header');
        $this->load->view('komponen_admin/sidebar');
        $this->load->view('admin/chat',$data);
        $this->load->view('komponen_admin/footer');
    }

    public function tampilChat($id)
    {
        // $id_user = $this->session->userdata('id_user');
        $data['chat'] = $this->ModelAdmin->getChat($id);
        $data['chat2'] = $this->ModelAdmin->getChat($id);
        // $data['chat'] = $this->ModelAdmin->getChat();
        // $data['pesanan'] = json_decode($this->curl->simple_get($this->API.'/pesanan'));

        $this->load->view('komponen_admin/header');
        $this->load->view('komponen_admin/sidebar');
        $this->load->view('admin/userChat',$data);
        $this->load->view('komponen_admin/footer');
    }

    public function getPembayaran()
    {
        // $id_user = $this->session->userdata('id_user');
        $data['pembayaran'] = $this->ModelAdmin->getPembayaran();
        $data['total'] = $this->ModelAdmin->getTotal();
        // $data['pembayaran'] = json_decode($this->curl->simple_get($this->API.'/pembayaran'));

        $this->load->view('komponen_admin/header');
        $this->load->view('komponen_admin/sidebar');
        $this->load->view('admin/pembayaran',$data);
        $this->load->view('komponen_admin/footer');
    }

    public function chatAdmin(){
        $id = $this->input->post('id_penerima');
        // print_r($id); die;
        $data = array(
            'id_pengirim' => '2',
            'id_penerima' => $id,
            'id_conversation' => $id,
            'pesan' => $this->input->post('pesan')
                    );
        $this->ModelAdmin->chatAdmin($data);
        echo "<script>alert('Pesan Terkirim');history.go(-1);</script>";
    }

    public function insertBarang()
    {
        $config['upload_path'] = './picture/barang/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan


         $this->load->library('upload', $config);
         if (!$this->upload->do_upload('gambar')){
            $error = $this->upload->display_errors();
            print_r($error);
         }else{

            $result = $this->upload->data();
            $img_name = $result['file_name'];
            $data = array(
                            'gambar' => $img_name,
                            'nama' => $this->input->post('nama'),
                            'quantity' => $this->input->post('quantity'),
                            'harga' => $this->input->post('harga'),
                            'deskripsi' => $this->input->post('deskripsi')
                        );
            // $this->ModelAdmin->insertBarang($img_name); //akses model untuk menyimpan ke database
            $this->curl->simple_post($this->API.'/barang', $data, array(CURLOPT_BUFFERSIZE => 10)); 

            redirect('admin/getBarang');
            }
    }

     public function updateBarang()
    {
        
        $config['upload_path'] = './picture/barang'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan

        $id = $this->input->post('id');

         $this->load->library('upload', $config);
         if ( ! $this->upload->do_upload('gambar')){

        // $id = $this->input->post('account_id');
        // $data = array('account_id' => $id);
                    $data = array (
                        'gambar' => $this->input->post('gambar2'),
                        'nama' => $this->input->post('nama'),
                        'harga' => $this->input->post('harga'),
                        'quantity' => $this->input->post('quantity'),
                        'deskripsi' => $this->input->post('deskripsi')
                    );
                        // print_r($data_product); die;
                    $this->ModelAdmin->updateBarang($id,$data);
                    redirect('admin/getBarang');
         }else{
        // $id = $this->input->post('account_id');
        // $data = array('account_id' => $id);

            $image = $this->upload->data();
                    $data = array (
                        'id' => $this->input->post('id'),
                        'gambar' => $image['file_name'] ,
                        'nama' => $this->input->post('nama'),
                        'harga' => $this->input->post('harga'),
                        'quantity' => $this->input->post('quantity'),
                        'deskripsi' => $this->input->post('deskripsi')
                    );
                        // print_r($data_product); die;
                    $update =  $this->curl->simple_put($this->API.'/barang', $data, array(CURLOPT_BUFFERSIZE => 10)); 
                    // $this->ModelAdmin->updateBarang($id,$data);
                    redirect('admin/getBarang');
                }

    }

    public function deleteBarang($id)
    {
        // $where = array('id' => $id);
        // $this->ModelAdmin->deleteBarang($where);
        $this->curl->simple_delete($this->API.'/barang', array('id'=>$id), array(CURLOPT_BUFFERSIZE => 10)); 
        redirect('admin/getBarang');
    }

    public function updateStatus($id){
        $this->ModelAdmin->updateStatus($id);
        redirect('admin/getPembayaran');
    }

    public function updatePengiriman($id){
        $this->ModelAdmin->updatePengiriman($id);
        redirect('admin/getPesanan');
    }

}