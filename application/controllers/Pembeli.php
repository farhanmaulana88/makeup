<?php
Class Pembeli extends CI_Controller{
    
    var $API ="";
    
    function __construct() {
        parent::__construct();
        $this->API="http://localhost/makeupServer/index.php";
        $this->load->library('session');
        $this->load->library('cart');
        $this->load->library('curl');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');
        // $this->load->model('ModelAdmin');
        $this->load->model('ModelPembeli');

        if(
            $this->session->userdata('level') != "pembeli"){
            redirect(base_url("index.php/index/formLogin"));
            }
    }

    public function getBarang()
    {
        $id = $this->session->userdata('id');
        $data['chat'] = $this->ModelPembeli->getChat($id);
        $data['barang'] = json_decode($this->curl->simple_get($this->API.'/barang'));

        $this->load->view('komponen_pembeli/header2');
        $this->load->view('pembeli/index',$data);
        $this->load->view('komponen_pembeli/footer2',$data);
    }

    function detailProduk($id){
        $id2 = $this->session->userdata('id');
        $data['chat'] = $this->ModelPembeli->getChat($id2);
        $data['barang'] = $this->ModelPembeli->detailProduk($id);
        $data['barang2'] = $this->ModelPembeli->detailProduk($id);
        $data['barang3'] = $this->ModelPembeli->detailProduk($id);
        // $data['barang'] = json_decode($this->curl->simple_get($this->API.'/barang', $data));
        $data['komen'] = $this->ModelPembeli->getKomen($id);

        $this->load->view('komponen_pembeli/header');
        $this->load->view('pembeli/detailProduk',$data);
        $this->load->view('komponen_pembeli/footer', $data);
    }
    
    function openCart(){
            $id = $this->session->userdata('id');
            $data['chat'] = $this->ModelPembeli->getChat($id);
            $data['barang'] = json_decode($this->curl->simple_get($this->API.'/barang'));
            $this->load->view('komponen_pembeli/header');
            $this->load->view('pembeli/cart',$data);
            $this->load->view('komponen_pembeli/footer',$data);
    }

    function riwayatBelanja(){
        $id = $this->session->userdata('id');
        $data['chat'] = $this->ModelPembeli->getChat($id);
        $data['riwayat'] = $this->ModelPembeli->riwayatbelanja($id);

        $this->load->view('komponen_pembeli/header');
        $this->load->view('pembeli/riwayatbelanja',$data);
        $this->load->view('komponen_pembeli/footer',$data);
    }

    function riwayatStruk(){
        $id = $this->session->userdata('id');
        $data['chat'] = $this->ModelPembeli->getChat($id);
        $data['riwayat'] = $this->ModelPembeli->riwayatStruk($id);

        $this->load->view('komponen_pembeli/header');
        $this->load->view('pembeli/riwayatStruk',$data);
        $this->load->view('komponen_pembeli/footer',$data);
    }

    public function chatPembeli(){
        $data = array(
            'id_pengirim' => $this->session->userdata('id'),
            'id_penerima' => $this->input->post('id_penerima'),
            'id_conversation' => $this->session->userdata('id'),
            'pesan' => $this->input->post('pesan')
                    );
        $this->ModelPembeli->chatPembeli($data);
        echo "<script>alert('Pesan Terkirim');history.go(-1);</script>";
    }

    public function cart(){
        $data = array(
               'id'         => $this->input->post('id'),
               // 'account_id'          => $this->input->post('account_id'),
               'name'       => $this->input->post('nama'),
               'qty'                => $this->input->post('jumlah'),
               'gambar'      => $this->input->post('gambar'),
               'price'              => $this->input->post('harga')
            );
        $a=$this->cart->insert($data);
        // print_r($a);die;


        redirect(base_url('index.php/pembeli/openCart'));
}

function komen(){
    $b = $this->input->post('id_barang');
        $this->ModelPembeli->komen();
        redirect('pembeli/detailProduk/'.$b);
}

public function updateCart(){
     $cart_info = $_POST['cart'] ;
foreach( $cart_info as $id => $cart)
{
$rowid = $cart['rowid'];
$price = $cart['price'];
$gambar = $cart['gambar'];
$amount = $price * $cart['qty'];
$qty = $cart['qty'];
$data = array('rowid' => $rowid,
'price' => $price,
'gambar' => $gambar,
'amount' => $amount,
'qty' => $qty);
$this->cart->update($data);
}


        redirect(base_url('index.php/pembeli/openCart'));
}


public function clearCart(){
        $this->cart->destroy();
        redirect(base_url('index.php/pembeli/getBarang'));  
    }

    public function remove_item($id){
          $data = array(
            'rowid'   => $id,
            'qty'     => 0
        );
        $this->cart->update($data);
        redirect(base_url('index.php/pembeli/openCart'));  
    }

    
    public function order(){
    $is_processed = $this->ModelPembeli->order();
    foreach ($this->cart->contents() as $key) {
    // print_r($qty); die;

                $id = $key['id'];
                $qtyAwal = $this->db->get_where('tb_barang',array('id' => $id))->row();
                $qtyAwalFix = $qtyAwal->quantity;
                $qty = $key['qty'];
                $qtyAkhir = $qtyAwalFix - $qty;
                // print_r($qty);die;
    $stock = $this->ModelPembeli->stock($id,$qtyAkhir);
    }
        if($is_processed){
            $this->cart->destroy();
            redirect(base_url('index.php/pembeli/getBarang'));
        }else{
            $this->session->set_flashdata('error','Gagal untuk memproses pesanan anda, tolong coba lagi!');
            redirect(base_url('index.php/pembeli/getBarang')); 
        }
    }


    public function updateBukti()
    {
        
        $config['upload_path'] = './picture/bukti'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan

        $id = $this->input->post('id');

         $this->load->library('upload', $config);
         if ( ! $this->upload->do_upload('bukti')){
                    redirect('pembeli/riwayatStruk');
         }else{
        // $id = $this->input->post('account_id');
        // $data = array('account_id' => $id);

            $image = $this->upload->data();
                    $data = array (
                        'bukti' => $image['file_name']
                    );
                        // print_r($data_product); die;
                    $this->ModelPembeli->updateBukti($id,$data);
                    // $update =  $this->curl->simple_put($this->API.'/barang', $data, array(CURLOPT_BUFFERSIZE => 10)); 
                    redirect('pembeli/riwayatStruk');
                }

    }

    public function updatePengiriman($id){
        $this->ModelPembeli->updatePengiriman($id);
        redirect('pembeli/riwayatbelanja');
    }

}