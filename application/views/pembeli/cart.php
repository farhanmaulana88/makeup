<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->

		<div class="home">
			<div class="home_container d-flex flex-column align-items-center justify-content-end">
				<div class="home_content text-center">
					<div class="home_title">Shopping Cart</div>
					<div class="breadcrumbs d-flex flex-column align-items-center justify-content-center">
						<ul class="d-flex flex-row align-items-start justify-content-start text-center">
							<li><a href="#">Home</a></li>
							<li>Your Cart</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- Cart -->

		<div class="cart_section">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="cart_container">

							<!-- Cart Bar -->
							<div class="cart_bar">
								<ul class="cart_bar_list item_list d-flex flex-row align-items-center justify-content-end">
									<li class="mr-auto">Product</li>
									<li><center>Price</center></li>
									<li>Quantity</li>
									<li><center>Total</center></li>
									<li><center>Remove</center></li>
								</ul>
							</div>
							<?php
								        $i=0;
								        $total=0;
								        $grand_total = 0;
								        // $this->_cart_contents[$rowid]['rowid'] = $rowid;
								              foreach ($this->cart->contents() as $items):
								        $subtotal = ($items['qty'] * $items['price']);
								        $grand_total=$grand_total + $items['subtotal'];
								        $i++;
								      ?>

							<form action="<?= base_url();?>index.php/pembeli/updateCart" method="post">
							<!-- Cart Items -->
							<div class="cart_items">
								<ul class="cart_items_list">
									<input type="hidden" name="cart[<?php echo $items['id'];?>][id]" value="<?php echo $items['id'];?>" />
									<input type="hidden" name="cart[<?php echo $items['id'];?>][rowid]" value="<?php echo $items['rowid'];?>" />
									<input type="hidden" name="cart[<?php echo $items['id'];?>][name]" value="<?php echo $items['name'];?>" />
									<input type="hidden" name="cart[<?php echo $items['id'];?>][price]" value="<?php echo $items['price'];?>" />
									<input type="hidden" name="cart[<?php echo $items['id'];?>][gambar]" value="<?php echo $items['gambar'];?>" />
									<input type="hidden" name="cart[<?php echo $items['id'];?>][qty]" value="<?php echo $items['qty'];?>" />
									<!-- Cart Item -->
									<li class="cart_item item_list d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-end justify-content-start">
										<div class="product d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start mr-auto">
											<div><div class="product_number"><?= $i;?></div></div>
											<div><div class="product_image"><img src="<?php echo base_url() . 'picture/barang/' .$items['gambar']; ?>" alt=""></div></div>
											<div class="product_name_container">
												<div class="product_name"><?= $items['name'];?></a></div>
												<div class="product_text">Second line for additional info</div>
											</div>
										</div>
										<div> <span class="product_price product_text" id="price" name="price"><?= number_format($items['price'],2,',','.'); ?></span></div>
										<div class="product_quantity_container">
											<div>
												<input type="number" id="qty" name="cart[<?php echo $items['id'];?>][qty]" class="product_quantity ml-lg-auto mr-lg-auto text-center" value="<?= $items['qty'];?>">
											</div>
										</div>
										<div><span class="product_total product_text" id="subtotal"><?= number_format($subtotal,2,',','.'); ?></span></div>
										<div class="product_size product_text">
											<center>
												<a href="<?php echo base_url('index.php/pembeli/remove_item/'.$items['rowid']); ?>" class="remove">X</a>
											</center>
										</div>
									</li>
								</ul>
							</div>
							<?php endforeach; ?>

							<!-- Cart Buttons -->
							<div class="cart_buttons d-flex flex-row align-items-start justify-content-start">
								<div class="cart_buttons_inner ml-sm-auto d-flex flex-row align-items-start justify-content-start flex-wrap">
									<div><button class="button button_continue trans_200" type="submit">Update</a></div> &emsp;
								</form>
									<div class="button button_clear trans_200"><a href="<?= base_url();?>index.php/pembeli/clearCart">clear cart</a></div>
									<div class="button button_continue trans_200"><a href="<?= base_url();?>index.php/pembeli/getBarang">continue shopping</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row cart_extra_row">
					<div class="col-lg-6">
						<div class="cart_extra cart_extra_1">
							<div class="cart_extra_content cart_extra_coupon">
									<div class="cart_extra_title">Rekening</div>
								<div class="coupon_text"><img src="<?php echo base_url() . '/assets/images/rekening.png';?>" alt=""></div>
								<div class="shipping">
									<div class="cart_extra_title">Pengiriman Melalui</div>
									<ul>
											
								<div class="coupon_text"><img src="<?php echo base_url() . '/assets/images/pos.png';?>" alt=""></div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 cart_extra_col">
						<div class="cart_extra cart_extra_2">
							<div class="cart_extra_content cart_extra_total">
								<div class="cart_extra_title">Cart Total</div>
								<ul class="cart_extra_total_list">
									<li class="d-flex flex-row align-items-center justify-content-start">
										<div class="cart_extra_total_title">Subtotal</div>
										<div class="cart_extra_total_value ml-auto"><?= 'Rp. '.number_format($grand_total,0,',','.'); ?></div>
									</li>

									<li class="d-flex flex-row align-items-center justify-content-start">
										<div class="cart_extra_total_title">Total</div>
										<div class="cart_extra_total_value ml-auto"><?= 'Rp. '.number_format($grand_total,0,',','.'); ?></div>
									</li>
								</ul>
								<div class="checkout_button trans_200"><a href="<?php echo base_url()?>index.php/pembeli/order">proceed to checkout</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		