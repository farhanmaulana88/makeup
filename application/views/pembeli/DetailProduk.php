

	<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->

		<div class="home">
			<div class="home_container d-flex flex-column align-items-center justify-content-end">
				<div class="home_content text-center">
					<div class="home_title">Product Page</div>
					<div class="breadcrumbs d-flex flex-column align-items-center justify-content-center">
						<ul class="d-flex flex-row align-items-start justify-content-start text-center">
							<li><a href="#">Home</a></li>
							<li><a href="category.html">Woman</a></li>
							<li>New Products</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- Product -->

		<div class="product">
			<div class="container">
				<div class="row">
					
					<!-- Product Image -->
					<div class="col-lg-6">
						<div class="product_image_slider_container">
							<div id="slider" class="flexslider">
									<?php foreach ($barang as $barang) { ?>
								<ul class="slides">
										<img name="gambar" src="<?php echo base_url(). "/picture/barang/". $barang->gambar; ?>" />
									
								</ul>
							</div>
						</div>
					</div>
					<!-- Product Info -->
					<div class="col-lg-6 product_col">
						<div class="product_info">
					<form action="<?php echo base_url('index.php/pembeli/cart');?>" method="post">
										<input hidden name="gambar" value="<?php echo $barang->gambar ?>">
							<div class="product_name"><?= $barang->nama;?></div>
							<input type="text" hidden value="<?php echo $barang->nama ?>" name="nama">
							<input type="text" hidden value="<?php echo $barang->id ?>" name="id">
							<div class="product_rating_container d-flex flex-row align-items-center justify-content-start">
								<!-- <div class="rating_r rating_r_4 product_rating"><i></i><i></i><i></i><i></i><i></i></div> -->
								<!-- <div class="product_reviews_link"><a href="#">Reviews</a></div> -->
							</div>
							<div class="product_price"><span>Rp.<?= number_format($barang->harga,2,',','.'); ?></span></div>
							<input hidden name="harga" value="<?php echo $barang->harga ?>">
							<div class="product_category">Sisa Stok <span><?= $barang->quantity;?></a></div>
								<input type="text" hidden value="<?php echo $barang->quantity ?>" id="quantity" name="quantity">
							<div class="product_size">
								<div class="product_size_title">Pilih Jumlah</div>
								<!-- <div class="product_quantity ml-lg-auto mr-lg-auto text-center"> -->
												<input type="number" onkeyup="kali();" id="jumlah" name="jumlah" class="product_quantity ml-lg-auto mr-lg-auto product_text product_num"></input>
											<!-- </div> -->
							</div>
							<div class="product_text">
								<p><?php echo $barang->deskripsi;?></p>
							</div>
							<div class="product_buttons">
								<div class="text-right d-flex flex-row align-items-start justify-content-start">
									<div class="product_button product_cart text-center d-flex flex-column align-items-center justify-content-center">
										<div>
											<div><button type="submit"><img src="<?php echo base_url();?>assets/images/cart.svg" class="svg" alt=""></button><div>+</div></div></div>
									</div>
								</div>
							</div>
						</form>
							<?php } ?>
							</div>
						</div>
			<div class="row">
			<div class="col-xs-20">
				<div class="tabCommon mt-20">
					<div class="container2">
	<fieldset>
		<legend>Komentar</legend>
		<?php foreach ($komen as $komen) { ?>
		<div class="kotak">
			<span><?= $komen->nama_lengkap;?></span>
			<br><?= $komen->tanggal;?><hr>
			<p><?= $komen->komen;?></p>
		</div>
	<?php } ?>
	</fieldset>
</div>
					<!-- Input Comment -->
					<?php foreach ($barang2 as $key) { ?>
					<form method="post" action="<?php echo base_url('index.php/pembeli/komen')?>">
						<div class="form-group">
							<input type="text" style="width: 450px" placeholder="Write a comment" class="form-control" name="komen">
							<input type="text" hidden value="<?php echo $barang->id ?>" name="id_barang">
							<button class="btn btn-primary" type="submit">Send</button>
						</div>
					</form>
					<?php } ?>
					<!-- End Input	 -->
				</div>
			</div>
		</div>
					</div>
				</div>
			</div>
		</div>

		<script language="javascript">
function kali(){
      var jumlah = document.getElementById('jumlah').value;
      var quantity = document.getElementById('quantity').value;
      var x = parseInt(quantity);
      
      if(jumlah>x){
    alert('Maaf Pesanan Melebihi Batas!');
    document.getElementById('jumlah').value = "";
  }
}
</script>

		<!-- Boxes -->