<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->

		<div class="home">
			<div class="home_container d-flex flex-column align-items-center justify-content-end">
				<div class="home_content text-center">
					<div class="home_title">Shopping History</div>
					<div class="breadcrumbs d-flex flex-column align-items-center justify-content-center">
						<ul class="d-flex flex-row align-items-start justify-content-start text-center">
							<li><a href="<?php echo base_url();?>index.php/pembeli/getBarang">Home</a></li>
							<li>Your Shopping History</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- Cart -->

		<div class="cart_section">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="cart_container">

							<!-- Cart Bar -->
							<div class="cart_bar">
								<ul class="cart_bar_list item_list d-flex flex-row align-items-center justify-content-end">
									<li class="mr-auto">Product</li>
									<li><center>Price</center></li>
									<li>Quantity</li>
									<li><center>Total</center></li>
									<li><center>Pengiriman</center></li>
									<li><center>Update</center></li>
								</ul>
							</div>
							<?php
								        $i=0;
								        $total=0;
								        // $this->_cart_contents[$rowid]['rowid'] = $rowid;
								              foreach ($riwayat as $riwayat):
								        $subtotal = ($riwayat->harga * $riwayat->jumlah);
								        // $grand_total=$total + $items['']
								        $i++;
								      ?>

							<!-- Cart Items -->
							<div class="cart_items">
								<ul class="cart_items_list">

									<!-- Cart Item -->
									<li class="cart_item item_list d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-end justify-content-start">
										<div class="product d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start mr-auto">
											<div><div class="product_number"><?= $i;?></div></div>
											<div><div class="product_image"><img src="<?php echo base_url() . 'picture/barang/' .$riwayat->gambar; ?>" alt=""></div></div>
											<div class="product_name_container">
												<div class="product_name"><a href="product.html"><?= $riwayat->nama;?></a></div>
												<div class="product_text">Second line for additional info</div>
											</div>
										</div>
										<div class="product_price product_text"><span>Rp. </span><?= number_format($riwayat->harga,2,',','.'); ?></div>
										<div class="product_quantity_container">
											<div class="product_quantity ml-lg-auto mr-lg-auto text-center">
												<span class="product_text product_num"><?= $riwayat->jumlah;?></span>
											</div>
										</div>
										<div class="product_total product_text"><span>Rp. </span><?= number_format($subtotal,2,',','.'); ?></div>
										<div class="product_total product_text"><center><span><b><?= $riwayat->status;?></b></span></center></div>
										<?php if ($riwayat->status == 'Dikirim') { ?>
										<div class="product_total product_text"><center><a href='<?= base_url('index.php/pembeli/updatePengiriman/'.$riwayat->id);?>' class='btn btn-danger'><i class='fa fa-pencil'></i></a></center></div>
										<?php }else{ ?>
											<div class="product_total product_text"></div>
										<?php } ?>
									</li>
								</ul>
							</div>
							<?php endforeach; ?>

							<!-- Cart Buttons -->
							<div class="cart_buttons d-flex flex-row align-items-start justify-content-start">
								<div class="cart_buttons_inner ml-sm-auto d-flex flex-row align-items-start justify-content-start flex-wrap">
									<div class="button button_continue trans_200"><a href="<?= base_url();?>index.php/pembeli/getBarang">continue shopping</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row cart_extra_row">
					
				</div>
			</div>
		</div>