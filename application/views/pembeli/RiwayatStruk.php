<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->

		<div class="home">
			<div class="home_container d-flex flex-column align-items-center justify-content-end">
				<div class="home_content text-center">
					<div class="home_title">Invoice History</div>
					<div class="breadcrumbs d-flex flex-column align-items-center justify-content-center">
						<ul class="d-flex flex-row align-items-start justify-content-start text-center">
							<li><a href="<?php echo base_url();?>index.php/pembeli/getBarang">Home</a></li>
							<li>Your Invoice History</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- Cart -->

		<div class="cart_section">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="cart_container">

							<!-- Cart Bar -->
							<div class="cart_bar">
								<ul class="cart_bar_list item_list d-flex flex-row">
									<li class="mr-auto">Keterangan Pemesanan</li>
									<!-- <li>Alamat</li> -->
									<li style="width: 10%"><center>Id Pembayaran</center></li>
									<li>Bukti</li>
									<!-- <li><center>Total</center></li> -->
									<li><center>Total</center></li>
									<li><center>Status</center></li>
									<li><center>Update</center></li>
								</ul>
							</div>
							<?php
								        $i=0;
								        $total=0;
								        // $this->_cart_contents[$rowid]['rowid'] = $rowid;
								              foreach ($riwayat as $riwayat):
								        // $subtotal = ($riwayat->harga * $riwayat->jumlah);
								        // $grand_total=$total + $items['']
								        $i++;
								      ?>

							<!-- Cart Items -->
							<div class="cart_items">
								<ul class="cart_items_list">

									<!-- Cart Item -->
									<li class="cart_item item_list d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-end justify-content-start">
										<div class="product d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start mr-auto">
											<div><div class="product_number"><?= $i;?></div></div>
		
											<div class="product_name_container">
												<div class="product_name" style="font-size: 12px"><?= $riwayat->nama_lengkap;?>, <?= $riwayat->alamat;?></a></div>
											</div>
										</div>
										<!-- <td><center><textarea row="10" value=""><?= $riwayat->alamat;?></textarea></center></td> -->
										<div class="product_price product_text"><span>Rp. </span><?= $riwayat->id;?></div>
						
											<div><div class="product_image">
												<a href="<?php echo base_url() . 'picture/bukti/' .$riwayat->bukti; ?>" class="perbesar">
													<img id="imgtab" class='small' src="<?php echo base_url() . 'picture/bukti/' .$riwayat->bukti; ?>">
												</a>
											</div></div>

						
										<div class="product_total product_text">Rp. <?= number_format($riwayat->total,2,',','.'); ?></div>
										<div class="product_price product_text"><span>Rp. </span><?= $riwayat->status;?></div>
										<div class="product_price product_text"><button data-toggle='modal' data-target='#editBarang<?= $riwayat->id;?>' class="btn btn-sm btn-success" >Update</button></div>
									</li>
								</ul>
							</div>


							<div class="modal fade" id="editBarang<?php echo $riwayat->id;?>">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <i class="tf-ion-close"></i>
      </button>
        <div class="modal-dialog " role="document">
          <div class="modal-content">

             <div class="modal-header">
                            <h5 class="modal-title">Update Pembayaran</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
              <div class="modal-body">

                    <form action="<?php echo base_url(). 'index.php/pembeli/updateBukti'; ?>" method="post" enctype="multipart/form-data">

                    <input style="font-size: 15px" style="font-size: 15px" type="hidden" class="form-control" name="id" value="<?php echo $riwayat->id?>">

                <div class="col-md-6">

                    <div class="form-group">
                    <span>Bukti Transfer</span>
                    <input type="file" class="form-control" name="bukti">
                    </div>
                </div>
           
                      <button type="submit" class="btn btn-main">Edit</button>
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </form>
                  </div>
              </div>
          </div>
      </div>
							<?php endforeach; ?>

							<!-- Cart Buttons -->
						
						</div>
					</div>
				</div>
				<div class="row cart_extra_row">

					<div class="col-lg-6">
						<div class="cart_extra cart_extra_1">
							<div class="cart_extra_content cart_extra_coupon">
								<center><h3>No. Rekening Rossa Beauty</h3></center>
								<div class="coupon_text"><img src="<?php echo base_url() . '/assets/images/rekening.png';?>" alt=""></div>
								<div class="shipping">

									</ul>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-lg-6">
					<div class="cart_buttons d-flex flex-row align-items-start justify-content-start">
								<div class="cart_buttons_inner ml-sm-auto d-flex flex-row align-items-start justify-content-start flex-wrap">
									<div class="button button_continue trans_200"><a href="<?= base_url();?>index.php/pembeli/getBarang">continue shopping</a></div>
								</div>
							</div>
						</div>
				</div>


			</div>
		</div>