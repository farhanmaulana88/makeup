<br>
<br>

<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->

		<div class="home">
			<!-- Home Slider -->
			<div class="home_slider_container">
				<div class="owl-carousel owl-theme home_slider">
					
					<!-- Slide -->
					<div class="owl-item">
						<div class="background_image" style="background-image:url(<?php echo base_url()?>assets/images/rossaa.jpg)"></div>
						<div class="container fill_height">
							<div class="row fill_height">
								<div class="col fill_height">
									<div class="home_container d-flex flex-column align-items-center justify-content-start">
										<div class="home_content">
											<div class="home_title">Rossa Beauty</div>
											<div class="home_items">
												<div class="row">
													<div class="col-sm-3 offset-lg-1">
														<div class="home_item_side"><a href="product.html"><img src="<?php echo base_url()?>assets/images/lip_tease.jpg" alt=""></a></div>
													</div>
													<div class="col-lg-4 col-md-6 col-sm-8 offset-sm-2 offset-md-0">
														<div class="product home_item_large">
															<div class="product_tag d-flex flex-column align-items-center justify-content-center">
																<div>
																	<div>Best</div>
																	<div><span>Price!</span></div>
																</div>
															</div>
															<div class="product_image"><img src="<?php echo base_url()?>assets/images/lip_cream.jpg" alt=""></div>
															<div class="product_content">
																
																
															</div>
														</div>
													</div>
													<div class="col-sm-3">
														<div class="home_item_side"><a href="product.html"><img src="<?php echo base_url()?>assets/images/sorbet.jpg" alt=""></a></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>

				</div>
			</div>


			</div>

		<!-- Products -->

		<div class="products">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<div class="section_title text-center">Semua Makeup Populer</div>
					</div>
				</div>
				<div class="row products_row">
					
					<!-- Product -->
							<?php foreach ($barang as $barang) { ?>
					<div class="col-xl-4 col-md-6">
						<div class="product">
							<div class="product_image" ><img style="height: 300px; width: 500px" src="<?php echo base_url(). "/picture/barang/". $barang->gambar; ?>" alt=""></div>
							<div class="product_content">
								<div class="product_info d-flex flex-row align-items-start justify-content-start">
									<div>
										<div>
											<div class="product_name"><a href="<?php echo base_url('index.php/pembeli/detailProduk/'.$barang->id);?>"><?= $barang->nama;?></a></div>
										</div>
									</div>
									<div class="ml-auto text-right">
										<div>
										<!-- <div class="rating_r rating_r_4 home_item_rating"><i></i><i></i><i></i><i></i><i></i></div> -->
										<div class="product_price text-right"><span>Rp.<?= number_format($barang->harga,2,',','.'); ?></span></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
							<?php } ?>

				</div>
			</div>
		</div>
	</div>
				