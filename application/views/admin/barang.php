<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet" />

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-flash-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.1.3/r-2.1.0/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-flash-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.1.3/r-2.1.0/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.js"></script> -->


<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Kelola Barang</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>index.php/index/logout">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><button class="btn btn-success btn-fill" type="button" id="btn_input" data-toggle="modal" data-target="#addBarang"><i class="fa fa-plus"></i></button></h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="20"><center><b>Nama Barang</b></center></th>
                                            <th width="20"><center><b>Deskripsi</b></center></th>
                                            <th width="20"><center><b>Harga</b></center></th>
                                            <th width="20"><center><b>Quantity</b></center></th>
                                            <th width="20"><center><b>Gambar</b></center></th>
                                        </tr>
                                    </thead>
                                        <?php
                                        $no = 0;
                                        foreach ($barang as $barang) {
                                            $no++; ?>
                                    <tbody id="table-row">
                                        <tr>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $barang->nama; ?></b></center></td>
                                            <td><textarea name="deskripsi" id="deskripsi" value="" class="form-control border-input" required disabled cols="30" rows="10"><?php echo $barang->deskripsi?></textarea></td>
                                            <td width="20"><center><b>Rp.<?= number_format($barang->harga,2,',','.'); ?></b></center></td>
                                            <td width="20"><center><b><?= $barang->quantity; ?></b></center></td>
                                            <td width="20"><center><img style = "width:120px;height:130px" src="<?= base_url();?>picture/barang/<?= $barang->gambar;?>" alt=""></b></center></td>
                                            <td width="20"><center>
                                            <a class='btn btn-info' data-toggle='modal' data-target='#editBarang<?= $barang->id; ?>'><i class='fa fa-pencil'></i></a>    
                                            <a href='<?= base_url('index.php/admin/deleteBarang/'.$barang->id);?>' class='btn btn-danger'><i class='fa fa-trash'></i></a>
                                            </center></td>
                                            </tr>
                            
                                    </tbody>
                                            <div class="modal fade" id="editBarang<?php echo $barang->id;?>">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <i class="tf-ion-close"></i>
      </button>
        <div class="modal-dialog " role="document">
          <div class="modal-content">

             <div class="modal-header">
                            <h5 class="modal-title">Edit Jenis Kosmetik</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
              <div class="modal-body">

                    <form action="<?php echo base_url(). 'index.php/admin/updateBarang'; ?>" method="post" enctype="multipart/form-data">

                    <input style="font-size: 15px" style="font-size: 15px" type="hidden" class="form-control" name="id" value="<?php echo $barang->id?>">
                    <input style="font-size: 15px" style="font-size: 15px" type="hidden" class="form-control" name="gambar2" value="<?php echo $barang->gambar?>">

                    <div class="row">
                        <div class="col-md-6">
                    <div class="form-group">
                    <span>Nama Barang</span>
                    <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="nama" value="<?php echo $barang->nama?>">
                    </div>
                </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                    <span>Harga</span>
                    <input style="font-size: 15px" type="text" class="form-control" name="harga" value="<?php echo $barang->harga?>">
                    </div>
                </div>
                </div>

                    <div class="row">
                        <div class="col-md-6">
                    <div class="form-group">
                    <span>Quantity</span>
                    <input style="font-size: 15px" type="text" class="form-control" name="quantity" value="<?php echo $barang->quantity?>">
                    </div>
                </div>

                <div class="col-md-6">

                    <div class="form-group">
                    <span>Gambar</span>
                    <input type="file" class="form-control" name="gambar">
                    </div>
                </div>
                </div>
                <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Deskripsi</span>
                                        <textarea name="deskripsi" id="deskripsi" value="" class="form-control border-input" required cols="30" rows="10"><?php echo $barang->deskripsi?></textarea>
                                    </div>
                                </div>
                            </div> 
                      <button type="submit" class="btn btn-main">Edit</button>
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </form>
                  </div>
                </div>
              </div>
          </div>
        <?php } ?>
                                </table>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         



            <div id="addBarang" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Tambah Barang</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="add" action="<?= base_url();?>index.php/admin/insertBarang" method="post" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Barang</label>
                                        <input type="text" required id="nama" class="form-control border-input" placeholder="Nama Barang" value="" name="nama">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Harga</label>
                                        <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required id="harga" class="form-control border-input" placeholder="Harga" value="" name="harga">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Stok</label>
                                        <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required id="quantity" class="form-control border-input" placeholder="Quantity" value="" name="quantity">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Foto</label>
                                        <input type="file" name="gambar" id="gambar" required>
                                    </div>
                                </div>
                            </div>  
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Deskripsi</label>
                                        <textarea name="deskripsi" id="deskripsi" class="form-control border-input" required cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div> 
                        
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button id="btn_add" type="submit" class="btn btn-success btn-fill btn-wd">
                                    Tambah Barang
                                </button>
                                <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--  -->
        </div>
    </div>
</div>
