<!DOCTYPE html>
<html lang="en">
<head>
<title>Rossa Beauty</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Little Closet template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/styles/bootstrap-4.1.2/bootstrap.min.css');?>">
<link href="<?php echo base_url('assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/OwlCarousel2-2.2.1/owl.carousel.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/OwlCarousel2-2.2.1/owl.theme.default.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/OwlCarousel2-2.2.1/animate.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/styles/main_styles.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/styles/responsive.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/bootstrap-4.1.2/bootstrap.min.css">
<link href="<?php echo base_url();?>assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugins/flexslider/flexslider.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/product.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/product_responsive.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/bootstrap-4.1.2/bootstrap.min.css">
<link href="<?php echo base_url();?>assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/cart.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/styles/cart_responsive.css">
<link type="text/css" rel="<?php echo base_url();?>assets/fc/source.jquery.fancybox.css?v=2.1.5" media="screen">

<style type="text/css">
	{box-sizing: border-box;}

/* Button used to open the chat form - fixed at the bottom of the page */
.open-button {
  background-color: #555;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  opacity: 0.8;
  position: fixed;
  bottom: 23px;
  right: 28px;
  width: 100px;
}

/* The popup chat - hidden by default */
.form-popup {
  display: none;
  position: fixed;
  bottom: 0;
  right: 15px;
  border: 3px solid #f1f1f1;
  z-index: 9;
}

/* Add styles to the form container */
.form-container {
  max-width: 300px;
  padding: 10px;
  background-color: white;
}

/* Full-width textarea */
.form-container textarea {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
  resize: none;
  min-height: 200px;
}

/* When the textarea gets focus, do something */
.form-container textarea:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit/login button */
.form-container .btn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;
}

/* Add a red background color to the cancel button */
.form-container .cancel {
  background-color: red;
}

/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
</style>
		<style>
	*{margin:0; padding: 0}
		.container2{width: 800px; margin: auto;}
		fieldset{width: 450px; padding: 10px;}
		.kotak{border: 1px solid grey; padding: 5px; margin:5px 0; overflow: scroll; height: 300px}
</style>

</head>
<body>

<!-- Menu -->

<div class="menu">

	<!-- Navigation -->
	<div>
        <center><img src="<?= base_url();?>assets2/img/logo.jpg" style="width:200px;height:70px" alt=""></center>
    </div>
    <br>
	<div class="menu_nav">
		<ul>
			<li><a href="<?= base_url();?>index.php/pembeli/getBarang">Beranda</a></li>
			<li><a href="<?= base_url();?>index.php/pembeli/riwayatBelanja">Riwayat Belanja</a></li>
			<li><a href="<?= base_url();?>index.php/pembeli/riwayatStruk">Riwayat Struk</a></li>
		</ul>
	</div>
	<!-- Contact Info -->
	<div class="menu_contact">
		<div class="menu_phone d-flex flex-row align-items-center justify-content-start">
			<div><div><img src="<?php echo base_url()?>assets/images/phone.svg" alt="https://www.flaticon.com/authors/freepik"></div></div>
			<div>+1 912-252-7350</div>
		</div>
		<div class="menu_social">
			<ul class="menu_social_list d-flex flex-row align-items-start justify-content-start">
				<li>
                    <a href="<?= base_url();?>index.php/index/logout">
						<i class="fa fa-sign-out"></i>
                    </a>
                </li>
			</ul>
		</div>
	</div>
</div>

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<div class="header_overlay"></div>
		<div class="header_content d-flex flex-row align-items-center justify-content-start">
			<div class="logo">
				<a href="<?= base_url();?>index.php/pembeli/getBarang">
					<div class="d-flex flex-row align-items-center justify-content-start">
						<div>
							<img src="<?php echo base_url()?>assets2/img/logo.jpg" style="width:200px;height:70px" alt="">
						</div>
					</div>
				</a>	
			</div>
			<div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
			<nav class="main_nav">
				<ul class="d-flex flex-row align-items-start justify-content-start">
					<li><a href="<?= base_url();?>index.php/pembeli/getBarang">Beranda</a></li>
					<li><a href="<?= base_url();?>index.php/pembeli/riwayatBelanja">Riwayat Belanja</a></li>
					<li><a href="<?= base_url();?>index.php/pembeli/riwayatStruk">Riwayat Struk</a></li>
				</ul>
			</nav>
			<div class="header_right d-flex flex-row align-items-center justify-content-start ml-auto">

							<!-- Chat -->
				
				<!-- Cart -->
				<div class="cart"><a href="<?= base_url();?>index.php/pembeli/openCart"><div><img class="svg" src="<?php echo base_url()?>assets/images/cart.svg" alt="https://www.flaticon.com/authors/freepik"></div></a></div>
				<!-- Phone -->
				<div class="header_phone d-flex flex-row align-items-center justify-content-start">
					<div>
						<div>
							<img src="<?php echo base_url()?>assets/images/phone.svg" alt="https://www.flaticon.com/authors/freepik">
						</div>
					</div>
					<div>+1 912-252-7350</div>
				</div>
			</div>

		</div>
	</header>