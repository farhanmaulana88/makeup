<!DOCTYPE html>
<html lang="en">
<head>
<title>Little Closet</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Little Closet template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/styles/bootstrap-4.1.2/bootstrap.min.css');?>">
<link href="<?php echo base_url('assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/OwlCarousel2-2.2.1/owl.carousel.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/OwlCarousel2-2.2.1/owl.theme.default.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/OwlCarousel2-2.2.1/animate.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/styles/main_styles.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/styles/responsive.css');?>">

</head>
<body>

<!-- Menu -->


<div class="super_container">

	<!-- Header -->

	<header class="header">
		<div class="header_overlay"></div>
		<div class="header_content d-flex flex-row align-items-center justify-content-start">
			<div class="logo">
				<a href="<?= base_url();?>index.php/pembeli/getBarang">
					<div class="d-flex flex-row align-items-center justify-content-start">
						<div><img src="<?php echo base_url()?>assets/images/logo_1.png" alt=""></div>
						<div>Little Closet</div>
					</div>
				</a>	
			</div>
			<div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
			<nav class="main_nav">
				<ul class="d-flex flex-row align-items-start justify-content-start">
					<li><a href="<?= base_url();?>index.php/pembeli/getBarang">Beranda</a></li>
			<li><a href="<?= base_url();?>index.php/pembeli/riwayatBelanja">Riwayat Belanja</a></li>
			<li><a href="<?= base_url();?>index.php/pembeli/riwayatStruk">Riwayat Struk</a></li>
				</ul>
			</nav>
			<div class="header_right d-flex flex-row align-items-center justify-content-start ml-auto">


				<!-- Login -->
				<div class="cart"><a href="<?= base_url();?>index.php/index/formLogin"><div><img style="height: 30px; width: 45px;" class="svg" src="<?php echo base_url()?>assets/images/login.jpg" alt="Login"></div></a></div>
				<!-- Cart -->
				<div class="cart"><a href="<?= base_url();?>index.php/pembeli/openCart"><div><img class="svg" src="<?php echo base_url()?>assets/images/cart.svg" alt="https://www.flaticon.com/authors/freepik"></div></a></div>
				<!-- Phone -->
				<div class="header_phone d-flex flex-row align-items-center justify-content-start">
					<div><div><img src="<?php echo base_url()?>assets/images/phone.svg" alt="https://www.flaticon.com/authors/freepik"></div></div>
					<div>+1 912-252-7350</div>
				</div>
			</div>
		</div>
	</header>



<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->

		<div class="home">
			<!-- Home Slider -->
			<div class="home_slider_container">
				<div class="owl-carousel owl-theme home_slider">
					
					<!-- Slide -->
					<div class="owl-item">
						<div class="background_image" style="background-image:url(<?php echo base_url()?>assets/images/logorossa.jpg)"></div>
						<div class="container fill_height">
							<div class="row fill_height">
								<div class="col fill_height">
									<div class="home_container d-flex flex-column align-items-center justify-content-start">
										<div class="home_content">
											<div class="home_title">Rossa Beauty</div>
											<div class="home_items">
												<div class="row">
													<div class="col-sm-3 offset-lg-1">
														<div class="home_item_side"><a href=""><img src="<?php echo base_url()?>assets/images/lip_tease.jpg" alt=""></a></div>
													</div>
													<div class="col-lg-4 col-md-6 col-sm-8 offset-sm-2 offset-md-0">
														<div class="product home_item_large">
															<div class="product_tag d-flex flex-column align-items-center justify-content-center">
																<div>
																	<div>Best</div>
																	<div><span>Price!</span></div>
																</div>
															</div>
															<div class="product_image"><img src="<?php echo base_url()?>assets/images/lip_cream.jpg" alt=""></div>
															<div class="product_content">
																
																
															</div>
														</div>
													</div>
													<div class="col-sm-3">
														<div class="home_item_side"><a href=""><img src="<?php echo base_url()?>assets/images/sorbet.jpg" alt=""></a></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>

				</div>
			</div>


			</div>

		<!-- Products -->

		<div class="products">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<div class="section_title text-center">Semua Makeup Populer</div>
					</div>
				</div>
				<div class="row products_row">
					
					<!-- Product -->
							<?php foreach ($barang as $barang) { ?>
					<div class="col-xl-4 col-md-6">
						<div class="product">
							<div class="product_image" ><img style="height: 300px; width: 500px" src="<?php echo base_url(). "/picture/barang/". $barang->gambar; ?>" alt=""></div>
							<div class="product_content">
								<div class="product_info d-flex flex-row align-items-start justify-content-start">
									<div>
										<div>
											<div class="product_name"><a href="<?php echo base_url('index.php/pembeli/detailProduk/'.$barang->id);?>"><?= $barang->nama;?></a></div>
										</div>
									</div>
									<div class="ml-auto text-right">
										<div>
										<!-- <div class="rating_r rating_r_4 home_item_rating"><i></i><i></i><i></i><i></i><i></i></div> -->
										<div class="product_price text-right"><span>Rp.<?= number_format($barang->harga,2,',','.'); ?></span></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
							<?php } ?>

				</div>
			</div>
		</div>
	</div>
				


			<!-- Footer -->

		<footer class="footer">
			<div class="footer_content">
				<div class="container">
					<div class="row">

						<!-- Footer Contact -->
					</div>
				</div>
			</div>
			<div class="footer_bar">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="footer_bar_content d-flex flex-md-row flex-column align-items-center justify-content-start">
								<div class="copyright order-md-1 order-2"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
								<nav class="footer_nav ml-md-auto order-md-2 order-1">
									<ul class="d-flex flex-row align-items-center justify-content-start">
										<li><a href="<?= base_url();?>index.php/pembeli/getBarang">Beranda</a></li>
			<li><a href="<?= base_url();?>index.php/pembeli/riwayatBelanja">Riwayat Belanja</a></li>
			<li><a href="<?= base_url();?>index.php/pembeli/riwayatStruk">Riwayat Struk</a></li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
		
</div>

<script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js');?>"></script>
<script src="<?php echo base_url('assets/styles/bootstrap-4.1.2/popper.js');?>"></script>
<script src="<?php echo base_url('assets/styles/bootstrap-4.1.2/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/greensock/TweenMax.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/greensock/TimelineMax.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/scrollmagic/ScrollMagic.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/greensock/animation.gsap.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/greensock/ScrollToPlugin.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/OwlCarousel2-2.2.1/owl.carousel.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/easing/easing.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/progressbar/progressbar.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/parallax-js-master/parallax.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/custom.js');?>"></script>

</script>
</body>
</html>
				