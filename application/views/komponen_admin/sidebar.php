<?php
    $uri = $this->uri->segment(2);
?>
<div class="sidebar" data-background-color="white" data-active-color="danger">
    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <center><img src="<?= base_url();?>assets2/img/logo.jpg" style="width:120px;height:50px" alt=""></center>
                </a>
            </div>

            <ul class="nav">
    
            <?php
                if ($uri == 'getBarang') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>    
                    <a href="<?= base_url();?>index.php/admin/getBarang">
                        <i class="fa fa-cubes"></i>
                        <p>Barang</p>
                    </a>
                </li>
        

            <?php
                if ($uri == 'getPesanan') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getPesanan">
                        <i class="fa fa-inbox"></i>
                        <p>Data Pengiriman</p>
                    </a>
                </li>

            <?php
                if ($uri == 'getPembayaran') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getPembayaran">
                        <i class="fa fa-shopping-basket"></i>
                        <p>Data Pembayaran</p>
                    </a>
                </li>

            <?php
                if ($uri == 'getUser') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getUser">
                        <i class="fa fa-weixin"></i>
                        <p>Chat</p>
                    </a>
                </li>
                
            </ul>
    	</div>
    </div>